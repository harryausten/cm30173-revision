\documentclass{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{CM30173 Cryptography}}

    {\large \textbf{2014 Paper}}
\end{center}

\begin{enumerate}
    % Question 1
    \item
        \begin{enumerate}

            % Part A
            \item What is meant by \textit{Confusion} and \textit{Diffusion} as properties of an encryption system?

                \begin{itemize}
                    \item \textit{Confusion} - The relationship between the key and the ciphertext is as complex as possible.
                    \item \textit{Diffusion} - The relationship between the plaintext and the ciphertext is as complex as possible.
                \end{itemize}

            % Part B
            \item Explain how a \textit{substitution-permutation network} can be used to encrypt a block of plaintext.

                \begin{itemize}
                    \item A \textit{substitution-permutation network} encrypts a message by \texttt{XOR}ing it with a sequence of distinct keys (key sequence), applying an S-box $\pi_s$ and a permutation function $\pi_p$ in an iterated block cipher algorithm that performs each of these components once on each round, forming what is called the round function. The S-box creates confusion and the permutations and key mixing create diffusion.
                \end{itemize}

            % Part C
            \item Describe how \textit{differential cryptanalysis} may be used to attack a SPN-based encryption system. Your answer should identify the goal of the attack and resources required, and include explanation of the following terminology: propagation ratio, differential trail, candidate key, right pair.

                \begin{itemize}
                    \item \textit{Differential cryptanalysis} is a chosen plaintext attack, aiming to obtain targetted bits of the final round key.
                    \item Plaintexts are chosen in pairs such that $ x \oplus x^* = x' $, a particular plaintext difference, which is chosen such that with high probability, during encryption, a particular state difference will occur at the input to the last round of S-boxes.
                    \item We find the expected difference between the state and $ \operatorname{state}^* $ just before the final S-boxes by considering all possible differentials $ (a',b') = $ (input difference, output difference) for each S-box, calculating the propagation ratio (the probability $b'$ occurs given $a'$) and combine high probability differentials to construct a differential trail.
                    \item Candidate keys are partial keys that vary the bits interacting with the differential trail.
                    \item Not all tuples $(x,x^*,y,y^*)$ will be useful for this particular difference trail, therefore we filter the input tuples to output \textit{right pairs}.
                \end{itemize}

            % Part D
            \item Why is \textit{electronic code book} not considered a secure mode of operation for block cipher encryption? Define an alternative, more secure mode of operation.

                \begin{itemize}
                    \item Using \textit{ECB}, identical blocks of plaintextare encrypted as identical blocks of ciphertext (i.e. it lacks diffusion).
                    \item An alternative, more secure mode of operation is \textit{CBC} (\textit{cipher block chaining}), where each block of plaintext is XORed with the previous ciphertext block before encryption. This way, each ciphertext block is influenced by previous plaintext blocks, introducing diffusion.
                \end{itemize}

        \end{enumerate}

    % Question 2
    \item
        \begin{enumerate}

            % Part A
            \item What is a cryptographic hash function? Give an example of a NIST-approved hash function.

                \begin{itemize}
                    \item A \textit{cryptographic hash function} is an easily computed function mapping data of arbitrary length to a fixed length output.
                    \item Examples of \textit{NIST-approved} (\textit{National Institute of Standards \& Technology}) hash functions are \texttt{SHA-2} or \texttt{SHA-3}
                \end{itemize}

            % Part B
            \item What is a \textit{collision} for an unkeyed hash function? What does it mean for such a function to be \textit{collision-resistant}? Does collision resistance imply \textit{preimage resistance} (explain)?

                \begin{itemize}
                    \item A \textit{collision} for a hash function $ h : \mathbb{X} \rightarrow \mathbb{Y} $ is a pair of distinct values $ x \neq x' $ such that $ h(x) = h'(x) $.
                    \item A hash function $h$ is \textit{collision resistant} if finding a collision requires approximately $\sqrt{\lvert \mathbb{Y} \rvert}$ operations.
                    \item Collision resistance $\not\Rightarrow$ preimage resistance, since finding a pre-image by brute force requires $\lvert \mathbb{Y} \rvert$ operations, so any function which yields pre-images more quickly is not pre-image resistant, but may be collision resistant.
                \end{itemize}

            % Part C
            \item What is meant by the following data security properties: \textit{integrity} and \textit{message authenticity}? Explain precisely how Alice and Bob may use a \textit{message authentication code} to maintain these properties when sending and receiving unencrypted messages over an unsecured channel.

                \begin{itemize}
                    \item \textit{Integrity} - Only authorised principals may alter data
                    \item \textit{Message Authenticity} - Assurance that the source of a message is accurately represented.
                    \item Alice may assure authenticity and integrity by sending Bob $(m,h_k(m))$, where $h$ is a \textit{message authentication code} and $k$ is their shared private key. If Bob receieves $(m,y)$, he can compute $h_k(m)$ and compare it with $y$ to validate that Alice sent the message.
                \end{itemize}

            % Part D
            \item Suppose Alice and Bob agree to define a message authentication code using an unkeyed hash function $h$ (derived using the Merkle-Damgard construction) and a shared secret key $k$, by defining $ h_k(m) = (k \Vert m) $. Why is this not secure (explain any attack)? How should Alice and Bob use h to define a MAC?

                \begin{itemize}
                    \item This attempt to define a MAC is vulnerable to length extension attack — an attacker who knows the length of $ k \Vert m $ may apply the compression function to $ h(k,m) $ and an extension $ m' $ of his choice, padded with the total length of $ k \Vert m \Vert m' $ to obtain a valid authentication tag for $ m \Vert m' $ without knowledge of $ k $.
                    \item A secure HMAC may be obtained as $ h_k(m) = h( k \oplus opad \Vert h(k \oplus ipad \Vert m) ) $.
                \end{itemize}

        \end{enumerate}

    % Question 3
    \item
        \begin{enumerate}

            % Part A
            \item Using the product of the prime factors 17 and 13 as a modulus for RSA encryption, and 5 as the public key exponent, calculate the private key exponent.

                \begin{itemize}
                    \item Encryption modulus is $ 17 \cdot 13 = 221 $. Its totient is $ 16 \cdot 12 = 192 $, so we need to find the inverse of 5 in the multiplicative group of integers modulo 192, which is 77 (by extended Euclidean algorithm).
                \end{itemize}

            % Part B
            \item Why is a low value such as 3 a poor choice of public key exponent? (explain any attack).

                \begin{itemize}
                    \item Hastad's broadcast attack could be used to break the encryption if the message had been broadcasted to multiple people.
                    \item The attacker would intercept all three ciphertexts: $ y_1 = x^3 \operatorname{mod} n_1 $, $ y_2 = x^3 \operatorname{mod} n_2 $ and $ y_3 = x^3 \operatorname{mod} n_3 $.
                    \item The attacker can solve $ Y = y_1 \operatorname{mod} n_1 = y_2 \operatorname{mod} n_2 = y_3 \operatorname{mod} n_3 $ for $Y$, using the Chinese Remainder Theorem.
                    \item The solution would then be $ Y \operatorname{mod} n_1 n_2 n_3 $ however $ x^3 < n_1 n_2 n_3 $ therefore we must have that $ x^3 = Y $, hence we can compute the original message $ x = \sqrt[3]{Y} $.
                \end{itemize}

            % Part C
            \item Explain what is meant by semantic security of a public key encryption system. Is basic RSA encryption semantically secure?

                \begin{itemize}
                    \item Semantic security: A computationally bounded attacker who knows only the public key should not be able to derive any information about an encrypted message.
                    \item Basic RSA encryption is not semantically secure becuase of its malleability and determinacy.
                \end{itemize}

            % Part D
            \item Show how knowledge of an RSA decryption exponent can be used to factor the modulus of encryption. Does this entail that solving the RSA problem is as hard as integer factorization?

                \begin{itemize}
                    \item Given the encryption and decryption exponents, $a$ and $b$, we can use the fact that $x^{ab} - 1 \equiv 1 \operatorname{mod} n $, $ \forall \ x \in \mathbb{Z}^*_n $ (the set of residues modulo $n$ that are relatively prime to $n$), so by repeatedly taking square roots, for trial initial values of $x$, we find $ y = x^{(ab-1) / 2^s} $ such that $ y^2 \equiv 1 \operatorname{mod} n $ and $ y \not\equiv 1 \operatorname{mod} n $, and hence $ y + 1 $, $ y - 1 $ share non-trivial factors with $n$.
                    \item The RSA problem is not equivalent to integer factorization, as it only requires the efficient recovery of the plaintext of a message from the ciphertext using only the public key.
                \end{itemize}

        \end{enumerate}

    % Question 4
    \item
        \begin{enumerate}

            % Part A
            \item Describe the El-Gamal digital signature scheme, including key generation, signature generation and verification.

                \begin{itemize}
                    \item \textbf{Setup} - Let $h$ be a collision-resistant hash function. Let $p$ be a large prime such that computing discrete logarithms modulo $p$ is difficult. Let $ g < p $ be a randomly chosen generator of the multiplicative group of integers modulo $p$, $ \mathbb{Z}^*_p $.
                    \item \textbf{Key Generation} - Randomly choose a secret key $x$ with $ 1 < x < p - 2 $. Compute the public key $ y = g^x \bmod p $.
                    \item \textbf{Signature Generation} - Choose a random $k$ such that $ 1 < k < p - 1 $ and $ \operatorname{gcd}(k, p - 1) = 1 $. Compute $ r \equiv g^k \bmod p $ and $ s \equiv ( h ( m ) - x r ) k^{-1} \bmod (p - 1) $. If $ s = 0 $ start over again. Then the pair $ (r,s) $ is the digital signature of $m$.
                    \item \textbf{Verification} - A signature $ (r,s) $ of a message $m$ is verified if and only if $ 0 < r < p $, $ 0 < s < p - 1 $ and $ g^{h(m)} \equiv y^rr^s \bmod p $. Otherwise it is rejected.
                \end{itemize}

            % Part B
            \item Prove that the signature generated by the signing algorithm is accepted according to the verification procedure.

                \begin{itemize}
                    \item The signature generation implies $ h( m ) \equiv xr + sk \bmod ( p - 1 ) $. Hence Fermat's little theorem implies $ g^{h(m)} \equiv g^{xr} g^{ks} \equiv ( g^x )^r ( g^k )^s \equiv ( y )^r ( r )^s \bmod p $.
                \end{itemize}

            % Part C
            \item Give a necessary condition on the choice of parameters when \textit{generating keys} and \textit{signing messages} if the scheme is to be secure in use.

                \begin{itemize}
                    \item When generating keys, a multiplicative group should be chosen such the that there are no known efficient methods of finding discrete logarithms.
                    \item When signing messages, a different ephemeral (short-term) key should be chosen each time.
                \end{itemize}

            % Part D
            \item Show how an attacker can create a key-only existential forgery in the basic scheme. How is this prevented in practice?

                \begin{itemize}
                    \item An attacker with a public key $ (p, \alpha, \beta) $ (such that $ \beta \equiv \alpha^a \bmod p $ for an unknown private key $a$) can create an existential forgery of a signature by choosing $ 0 \leq i, j \leq p - 2 $, and taking $ \gamma = \alpha^i \beta^j \bmod p $, $ \delta = - \gamma j^{-1} \bmod (p - 1) $ and $ x = i\delta \bmod (p - 1) $. Then $ \beta^\gamma \gamma^\delta \equiv \beta^\gamma \alpha^{(i+aj)\delta} \bmod p \equiv \beta^\gamma \alpha^{i\delta+aj\delta} \bmod p \equiv \beta^\gamma \alpha^{x - a\gamma} \bmod p \equiv \beta^{\gamma - \gamma} x^a \bmod p \equiv x^a \bmod p $.
                    \item This forgery is prevented by hashing any message before signing.
                \end{itemize}

        \end{enumerate}

\end{enumerate}

\end{document}
