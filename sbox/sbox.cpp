#include <iostream>
#include <fstream>

// ASCII character code S-box (32 - 126)
static uint8_t S[95] = { 113, 42, 33, 118, 94, 66, 58, 38, 67, 120, 64, 117, 60, 124, 69, 122, 103, 102, 53, 109, 59, 32, 125, 49, 89, 90, 92, 41, 110, 34, 88, 104, 40, 48, 93, 68, 101, 62, 126, 78, 76, 116, 91, 50, 105, 56, 83, 70, 121, 72, 55, 80, 107, 123, 96, 35, 95, 84, 65, 82, 81, 36, 86, 61, 112, 100, 87, 44, 57, 74, 54, 111, 106, 43, 51, 75, 63, 39, 98, 71, 79, 119, 52, 77, 115, 97, 108, 46, 45, 85, 114, 47, 99, 73, 37 };

void sub( char & c )
{
    // Adjust ASCII code to start of array
    // ASCII characters before 32 and after 126 are special
    // characters so we do not want them
    c = S[c - 32];
}

void x_or( const char & c1, char & c2 )
{
    c2 = (((c1 - 32) + (c2 - 32)) % 95) + 32;
}

std::string ecb( const std::string & in )
{
    std::string out( in );

    for ( size_t i = 0; i < out.size(); i++ )
    {
        sub( out[i] );
    }

    return out;
}

std::string cbc( const std::string & in, char & y )
{
    std::string out( in );

    for ( size_t i = 0; i < out.size(); i++ )
    {
        x_or( y, out[i] );
        sub( out[i] );
        y = out[i];
    }

    return out;
}

int main()
{
    std::ifstream ifile( "input.txt" );
    std::ofstream ecbfile( "output_ecb.txt", std::ofstream::trunc );
    std::ofstream cbcfile( "output_cbc.txt", std::ofstream::trunc );

    char y = 'A';

    while ( !ifile.eof() )
    {
        std::string in;
        std::string out;
        std::getline( ifile, in );

        ecbfile << ecb( in ) << std::endl;
        cbcfile << cbc( in, y ) << std::endl;
    }

    ifile.close();
    ecbfile.close();
    cbcfile.close();

    return 0;
}
