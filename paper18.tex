\documentclass{article}

\usepackage[margin=15mm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{centernot}

\newcommand{\X}{\mathcal{X}}
\newcommand{\Y}{\mathcal{Y}}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{CM30173 Cryptography}}

    {\large \textbf{2018 Paper}}
\end{center}

\begin{enumerate}

    % Question 1
    \item
        \begin{enumerate}
            % Part A
            \item What is a substitution-permutation network? How is it used to encrypt a block of plaintext?
                \begin{itemize}
                    \item An \textbf{iterated block cipher} is a block cipher (i.e. it splits up the plaintext $x$ into blocks $x_1,x_2,\hdots$) containing:
                        \begin{itemize}
                            \item A \textbf{key schedule} of $N_r$ \textbf{round keys}, $(k^1,k^2,\hdots,k^{N_r})$, derived using a fixed public algorithm from the key $k$
                            \item A \textbf{round function}, $f$, which takes a round key and a current \textbf{state}
                        \end{itemize}
                        The encryption and decryption functions consist of $N_r$ repetitions of the round function $f$
                    \item A \textbf{substitution-permutation network} is an iterated block cipher where the round function $f$ is a combination of XORing with the current round key $k^r$, applying a substitution function $\pi_s$ and applying a permutation function $\pi_p$. The penultimate round XORs the round key and applies $\pi_s$ but not $\pi_p$ and the final round only XORs the round key.
                \end{itemize}
                Explain how the same encryption algorithm for an SPN can be used for decryption by describing the required changes to its components.
                \begin{itemize}
                    \item Decryption is very simple. The substitution must be a bijection to allow it to be inverted, then we can simply do the encryption process using $\pi_p^{-1}$ as the permutation function, $\pi_s^{-1}$ as the substitution function, and the round keys in reverse order (with all but the first and last keys permuted).
                \end{itemize}
            % Part B
            \item How would you design an SPN which is resistant to differential cryptanalysis? (give the required properties of its components, justifying your answer)
                \begin{itemize}
                    \item S-boxes need to have low bias $\implies$ differential trails have low propagation values
                    \item Permutations that produces wide differential trails prevent the effectiveness of differential cryptanalysis, as it is harder to find the correct trail, due to small propagation values
                \end{itemize}
                Which data would an adversary require to mount this attack?
                \begin{itemize}
                    \item An adversary requires ciphertexts for chosen pairs of plaintext with a particular plaintext difference
                \end{itemize}
                How would you estimate how much data would be required?
                \begin{itemize}
                    \item If $p$ is the propagation ratio of the differential trail then the attack is often successful if the number of tuples $(x, x^* , y, y^* )$ is approximately $c/p$ where $c$ is some small constant
                \end{itemize}
            % Part C
            \item Explain why \textbf{electronic codebook} is not considered a secure mode of operation for block ciphers
                \begin{itemize}
                    \item Electronic codebook mode is simply a single direct application of the SPN on each block of plaintext, hence any identical plaintext blocks will be mapped to the same ciphertext, making it easier for the adversary to relate the plaintext and ciphertext (i.e. lacks diffusion)
                \end{itemize}
                Define the encryption of a sequence of plaintext blocks $x_1, \hdots, x_n $, using encryption function $e_k$ in cipherblock chaining mode. Define the CBC decryption of ciphertext blocks $ y_1, \hdots, y_n $ using decryption function $d_k$, and prove that if $ d_k(e_k(x)) = x $ for all $x$, then CBC encryption followed by CBC decryption returns the plaintext.
                \begin{itemize}
                    \item Cipherblock chaining mode XORs the output of the previous block encryption with the input to the next block. The encryption is defined as
                        \begin{equation*}
                            y_i = e_k(y_{i-1} \oplus x_i) \quad , 1 \le i \le n
                        \end{equation*}
                        where $IV = y_0$ is an extra arbitrary input, often all 0s. Inverting this for decryption, we get
                        \begin{equation*}
                            x_i = d_k(y_i) \oplus y_{i-1} \quad , 1 \le i \le n
                        \end{equation*}
                \end{itemize}
        \end{enumerate}

    % Question 2
    \item
        \begin{enumerate}
            % Part A
            \item Define the notion of a \textbf{(unkeyed) cryptographic hash function}
                \begin{itemize}
                    \item An \textbf{unkeyed hash function} is a function $ h : \mathcal{X} \rightarrow \mathcal{Y} $, where:
                        \begin{itemize}
                            \item $\mathcal{X}$ is the set of possible messages
                            \item $\mathcal{Y}$ is a finite set of possible \textbf{message dialects}
                        \end{itemize}
                        A pair $(x,y)$, $ x \in \mathcal{X} $, $ y \in \mathcal{Y} $ is valid if $ h(x) = y $
                \end{itemize}
                Give two applications of such a function, explaining the security properties it provides and why it provides them in each case.
                \begin{itemize}
                    \item Unkeyed hash functions are ideal for being used to store password hashes along with the associated usernames on servers, to provide protection against an attacker retrieving the information. The attacker would still have to try to invert the hash function, finding an $ x \in \cal{X} $ such that $ h(x) = y $
                    \item Another application of unkeyed hash functions is to verify message integrity. Message digests (hash digest over the entire message) calculated before and after transmission, are compared to determine whether any changes have been made to the message
                \end{itemize}
            % Part B
            \item What does it mean for a hash function to be \textbf{collision resistant}? (Explain your answer with reference to an attack for finding collisions and its complexity, which you should establish)
                \begin{itemize}
                    \item A \textbf{collision} occurs when there exists $ x,x' \in \mathcal{X}, x \neq x' $ such that $ h(x) = h(x') $
                \end{itemize}
                Does collision resistance imply pre-image resistance?
                \begin{itemize}
                    \item A hash function is \textbf{collision resistant} if the most efficient method for finding collisions is the birthday attack. If the hash function produces outputs of $n$ bits, then there are $2^n$ possible permutations of the $n$ bits, which implies that we can find a random collision in $O(\sqrt{2^n})$
                    \item A hash function is \textbf{preimage resistant} if it is hard to invert, i.e. given $ h : \X \rightarrow \Y $ and $ y \in \Y $, it is computationally infeasible to find an $ x \in \X $ such that $ h(x) = y $
                    \item A hash function is \textbf{second preimage resistant} if you cannot find a second input that maps to the same value as a given input value, i.e. given $ h : \X \rightarrow \Y $ and $ x \in \X $, it is computationally infeasible to find an $ x' \in \X $ such that $ h(x) = h(x') $
                    \item Hence, collision resistance is more general than second preimage resistance and not related to first preimage resistance
                        \begin{align*}
                            \text{collision resistance}
                            \begin{cases}
                                \centernot\implies \text{first preimage resistance} \\
                                \implies \text{second preimage resistance}
                            \end{cases}
                        \end{align*}
                \end{itemize}
            % Part C
            \item Explain how Alice and Bob would use a \textbf{message authentication code} to provide data integrity and message authenticity without confidentiality
                \begin{itemize}
                    \item Alice sends Bob $ (m,h_k(m)) $, where $m$ is her message and $k$, her shared key with Bob. If Bob receieves $ (m,h_k(m)) $, he knows that it was created by someone who knows $k$ (i.e. Alice) and has not been altered
                \end{itemize}
                Alice creates a MAC by prepending her message with a secret key and hashing the result with SHA-2. Explain why this is insecure by describing an attack which compromises it, and describe a way in which she could construct a MAC which is secure against this attack.
                \begin{itemize}
                    \item This is insecure as an adversary could perform a length extension attack on a hash function such as SHA-2 based on the Merkle-Damg\r{a}rd construction: \par
                        If a MAC is created by hashing a key concatenated with the message using a Merkle-Damg\r{a}rd construction, this can be used to create a MAC for an extension of the message (without knowing the key) by using it as the initial value in computing the hash of the extension
                    \item Alternatively, Alice could have used HMAC:
                        $$ \text{HMAC}_k(x) = h[(k \oplus \text{opad}) \Vert h(( k \oplus \text{ipad}) \Vert x)] $$
                        or CBC-MAC:
                        $$ y_i =
                        \begin{cases}
                            IV &, i = 0 \\
                            e_k( y_{i-1} \oplus x_i ) &, i \ge 1
                        \end{cases}
                        $$
                \end{itemize}
        \end{enumerate}

    % Question 3
    \item
        \begin{enumerate}
            % Part A
            \item Alice wants to allow Bob to communicate with her using RSA encryption. She chooses $ 323 = 17 \cdot 19 $ to be her modulus of encryption, and 7 to be her encryption exponent. What is her private key?
                \begin{itemize}
                    \item $ n = 323, p = 17, q = 19, b = 7 $, therefore we can find the decryption exponent (private key) by using the extended Euclidean algorithm:
                        \begin{align*}
                            \phi(n) &= \phi(p \cdot q) \\
                            &= \phi(p) \cdot \phi(q) \\
                            &= 16 \cdot 18 \\
                            &= 288
                        \end{align*}
                        hence we are trying to find $a$ such that $ 7a \equiv 1 \bmod 288 $
                        \begin{align*}
                            288 &= 7(41) + 1
                        \end{align*}
                        therefore we have
                        \begin{align*}
                            1 &= 288 + 7(-41) \\
                            &\equiv 7 \cdot -41 \bmod 288 \\
                            &\equiv 7 \cdot 247 \bmod 288
                        \end{align*}
                        Which gives us our decryption exponent (private key) equal to $ a = \underline{\underline{247}} $
                \end{itemize}
                Bob uses the public key to encrypt the message 18: What value does he send to Alice?
                \begin{itemize}
                    \item Bob encrypts $ x = 18 $ and sends
                        \begin{align*}
                            y &= x^b \bmod n \\
                            &= 18^7 \bmod 323 \\
                            &= 18
                        \end{align*}
                        since $ 18^2 = 324 \equiv 1 \bmod 323 $
                \end{itemize}
                How would Alice decrypt this ciphertext?
                \begin{itemize}
                    \item Alice decrypts $ y = 18 $ by calculating
                        \begin{align*}
                            x &= y^a \bmod n \\
                            &= 18^{247} \bmod 323 \\
                            &= 18
                        \end{align*}
                \end{itemize}
            % Part B
            \item Is it possible that it is easier to compute the totient of an RSA modulus than to factorize it? (Justify your answer)
                \begin{itemize}
                    \item No - $ \phi(p \cdot q) = (p-1) \cdot (q-1) $ gives a quadratic with roots equal to the factors of the modulus, i.e. if you solve one, then you solve the other
                \end{itemize}
                Show that if $ x^2 \equiv y^2 \bmod m $, $ x \not\equiv y \bmod m $ and $ x \not\equiv -y \bmod m $, then $ \text{gcd}(x-y,m) $ is a non-trivial factor for $m$
                \begin{itemize}
                    \item
                        \begin{align*}
                            x^2 \equiv y^2 \bmod m &\Leftrightarrow x^2 - y^2 \equiv 0 \bmod m \\
                            &\Leftrightarrow m | (x^2-y^2) \\
                            &\Leftrightarrow m | (x-y)(x+y) \\
                            \text{and} \quad x \not\equiv \pm y \bmod m &\Leftrightarrow
                            \begin{cases}
                                x-y \not\equiv 0 \bmod m \\
                                x+y \not\equiv 0 \bmod m
                            \end{cases} \\
                            &\Leftrightarrow
                            \begin{cases}
                                m \not| \ (x-y) \\
                                m \not| \ (x+y)
                            \end{cases}
                        \end{align*}
                \end{itemize}
                Use this to show that any method for deriving the decryption exponent from the public key can also be used to factor the modulus
                \begin{itemize}
                    \item Given $n,b$ and $a$, where by definition $ ba \equiv 1 \bmod \phi(n) $, we have
                        $$ ba-1=k\phi(n) \quad , \text{for some} \ k \in \mathbb{N} $$
                        Hence, for all $ x \in \mathbb{Z}^*_n $
                        $$ x^{ba-1} \equiv 1 \bmod n $$
                        Let
                        \begin{align*}
                            y_1 &:= x^{\frac{ba-1}{2}} \\
                            \implies y_1^2 &\equiv 1 \bmod n
                        \end{align*}
                        We want $ y_1^2 \equiv 1 \bmod n $ \textbf{and} $ y_1 \not\equiv 1 \bmod n $. Therefore, if $ y_1 \equiv 1 \bmod n $, try dividing the exponent by two and using $ y_2 = x^{\frac{ba-1}{4}} $ etc. \par
                        Once such a $y$ has been found, we can find a nontrivial factor of $n$ by computing the gcd
                        $$ \text{gcd}(y-1,n)=p $$
                \end{itemize}
                What is the RSA problem? Is solving it necessarily as hard as factorising the modulus?
                \begin{itemize}
                    \item The RSA problem is the attempt to recover $x$, given $ y = x^b \bmod n $, $n$ and $b$
                    \item There is no efficient algorithm known to solve this problem
                    \item The obvious approach is to factor $n$, compute $\phi(n)$ and compute $a$ as the multiplicative inverse of $b$ modulo $\phi(n)$
                    \item Therefore it is not known whether the RSA problem is as hard as factorisation, as there could be some easier unknown method. \textbf{As far as is publically known, solving RSA is as hard as factorisation}
                \end{itemize}
            % Part C
            \item Explain why basic RSA is not semantically secure, and how it can be made semantically secure in practice.
                \begin{itemize}
                    \item A cryptosystem is \textbf{semantically secure} if only negligible information about the plaintext can be feasibly extracted from the ciphertext
                    \item Basic RSA is deterministic, i.e. encrypting the same message twice will give the same ciphertext and so does not possess the ciphertext indistinguishability property (semantic security)
                    \item In practice, semantic security is obtained by Optimal Asymmetric Encryption Padding (OAEP)
                \end{itemize}
        \end{enumerate}

    % Question 4
    \item
        \begin{enumerate}
            % Part A
            \item Explain the steps of key generation, message signing and signature verification for the ElGamal digital signature scheme
                \begin{itemize}
                    \item Select $p$ prime and $\alpha$ a primitive element modulo $p$
                    \item Select a \textbf{private key} $a$ and compute $$ \beta = \alpha^a \bmod p $$
                    \item The public key is $(p,\alpha,\beta)$ and the private key is $a$
                    \item \textbf{Signing} - Pick a (secret) random number $ r \in \mathbb{Z}_{p-1}^* $ and compute
                        \begin{align*}
                            \gamma &= \alpha^r \bmod p , \\
                            \delta &= (x-a\gamma)r^{-1} \bmod (p-1) \\
                            \text{and} \quad \text{sig}_k(x,r) &= (\gamma,\delta)
                        \end{align*}
                    \item \textbf{Verifying}
                        \begin{equation*}
                            \text{ver}_k(x,(\gamma,\delta)) =
                            \begin{cases}
                                t &\beta^\gamma \gamma^\delta \equiv \alpha^x \bmod p \\
                                f &\text{otherwise}
                            \end{cases}
                        \end{equation*}
                \end{itemize}
                Show that it is correct - i.e. the verification procedure accepts precisely the correctly signed messages.
                \begin{itemize}
                    \item If $(\gamma,\delta)$ is a correct signature, then
                        \begin{alignat*}{2}
                            \delta &\equiv (x-a\gamma)r^{-1} &&\bmod (p-1) \\
                            \implies r\delta &\equiv x-a\gamma &&\bmod (p-1) \\
                            \implies \ x &\equiv a\gamma + r\delta &&\bmod (p-1)
                        \end{alignat*}
                        Hence, we have
                        \begin{alignat*}{2}
                            \beta^\gamma \gamma^\delta &\equiv \alpha^{a\gamma} \alpha^{r\delta} &&\bmod p \\
                            &\equiv \alpha^{a\gamma+r\delta} &&\bmod p \\
                            &\equiv \alpha^x &&\bmod p
                        \end{alignat*}
                \end{itemize}
            % Part B
            \item Show that the basic ElGamal digital signature scheme is susceptible to existential forgery and explain how this could be prevented.
                \begin{itemize}
                    \item \textbf{Existential forgery} is the creation of at least one message/signature pair $(m,\sigma)$, where $\sigma$ was not produced by the legitimate signer and the message $m$ is irrelevant
                    \item Choose $i,j \le p-2$, set $ \gamma = \alpha^i \beta^j $, $ \delta = -\gamma \cdot j^{-1} $ and $ x = i\delta $
                    \item Then
                        \begin{align*}
                            \beta^\gamma \gamma^\delta &= \beta^\gamma \left( \alpha^i \beta^j \right)^\delta \\
                            &= \beta^\gamma \left( \alpha^i \alpha^{aj} \right)^\delta \\
                            &= \beta^\gamma \alpha^{(i+aj)\delta} \\
                            &= \beta^\gamma \alpha^{aj\delta} \alpha^x \\
                            &= \beta^\gamma \beta^{j\delta} \alpha^x \\
                            &= \beta^\gamma \beta^{-\gamma} \alpha^x \\
                            &= \underline{\underline{\alpha^x}}
                        \end{align*}
                        as required
                    \item A common defence against this attack is to hash the messages before signing them
                \end{itemize}
            % Part C
            \item Describe a method of computing discrete logarithms in any cyclic group which is no less efficient than any other such general method. Establish its complexity
                \begin{itemize}
                    \item \textbf{Shank's Algorithm} is a meet-in-the-middle algorithm for computing the discrete logarithm, where we use the idea that the solution $a$ to the discrete logarithm problem $ \alpha^a = \beta \bmod n $ can be split into a quotient ($j$) and a remainder ($i$) with respect to the division by $\sqrt{n}$. Hence we can write the solution as $ a = mj+i $ where $ m = \sqrt{n} $
                    \item Since this algorithm takes $m$ steps in each direction (i.e. for each list), then the computational complexity of the algorithm is $O(\sqrt{n})$
                \end{itemize}
                What are the implications for key length in (e.g.) the Elliptic Curve Digital Signature Algorithm?
                \begin{itemize}
                    \item Because Shank's Algorithm takes $O(\sqrt{n})$ steps to find the solution to the discrete logarithm problem, the size of the key should be roughly twice the number of bits of security required for the signature
                \end{itemize}
        \end{enumerate}

\end{enumerate}

\end{document}
