#include <cmath>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <unordered_map>

// Modular exponentation using the square-multiply algorithm
uint32_t powm ( uint32_t base, uint32_t exp, const uint32_t & mod )
{
    // All numbers are 0 modulo 1
    if ( mod == 1 ) return 0;

    // Reduce base modulo mod
    base %= mod;

    uint32_t r = 1;

    while ( exp > 0 )
    {
        if ( exp & 1 )
        {
            r = r * base % mod;
        }

        base = base * base % mod;
        exp >>= 1;
    }

    return r;
}

// Determine the solution a of alpha^a = beta mod n
std::optional< uint32_t > shanks ( const uint32_t & alpha, const uint32_t & beta, const uint32_t & mod )
{
    const uint32_t m = ceil( sqrt(mod) );
    std::unordered_map< uint32_t, uint32_t > table = {};
    uint32_t e = 1;

    for ( uint32_t i = 0; i < m; ++i )
    {
        table[e] = i;
        e = e * alpha % mod;
    }

    const uint32_t fact = powm(alpha, mod-m-1, mod);
    e=beta;

    for ( uint32_t i = 0; i < m; ++i )
    {
        if ( auto it = table.find(e); it != table.end() )
        {
            return i*m + it->second;
        }

        e = e * fact % mod;
    }

    return std::nullopt;
}

// Input alpha, beta and n
int main ( int argc, char ** argv )
{
    if ( argc != 4 )
    {
        std::cerr << "Incorrect Input!" << std::endl;
    }

    std::array< uint32_t, 3 > args;

    for ( uint8_t i = 0; i < 3; i++ )
    {
        std::istringstream ss(argv[i+1]);
        ss >> args[i];
    }

    std::cout << shanks( args[0], args[1], args[2] ).value_or(0) << std::endl;

    return 0;
}
